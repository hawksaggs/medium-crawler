const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const urlSchema = new Schema({
    url:String,
    count:Number,
    parameters:String
},{
    timestamps:true
});

module.exports = mongoose.model("Url", urlSchema);
const mongoose = require('mongoose');
const dbURI = process.env.MONGODB_URL;

mongoose.connect(dbURI);
mongoose.connection.on('connected', function () {
    console.log('Mongoose connected to ' + dbURI);
});
mongoose.connection.on('error', function (err) {
    console.log('Mongoose connection error ' + err);
    process.exit(0);
});
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose disconnected ');
});
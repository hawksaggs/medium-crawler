const Url = require('../ models/urls');

module.exports = {
    save: (uniqueLinks) => {

        console.log(uniqueLinks);
        const urls = [];
        Object.keys(uniqueLinks).forEach(e => {
            var parameter = '';
            Object.keys(uniqueLinks[e].params).forEach(p => {
                parameter+= p + "|";
            });
            const obj = {
                'url': e,
                'count':uniqueLinks[e].count,
                'parameters':parameter
            };

            urls.push(obj);
        });
        console.log(urls);
        Url.insertMany(urls, (err, docs) =>{
            if (err) {
                console.log('error==========');
                console.log(err);
                console.error(err);
            }
            console.log(docs);
            return true;
        });
    }
}
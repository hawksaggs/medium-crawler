require('dotenv').config();
require('./db');
const request = require('request');
const cheerio = require('cheerio');
const urlService = require('./service/url');

var links = {};
var queue = [];
var maxConcurrency = 5;
var concurrency = 0;
var uniqueLinks = {};


var push = (url) => {
    queue.push(url);
    process();
}

var process = () => {
    if(queue.length == 0) {
        if(concurrency == 0) {
            console.log('scssd');
            console.log(uniqueLinks);
            return urlService.save(uniqueLinks);
        }
        return;
    };
    while(queue.length > 0 && concurrency < maxConcurrency) {
        console.log(concurrency + '==========================');
        concurrency++;
        scrape();
    }
}

var scrape = () => {
    const q = queue.shift();
    return scrapeWebsite(q, process);
}

var scrapeWebsite = (url, cb) => {
    request(url, function (error, response, body) {
        if (error) { return cb(error, url); }
        var $ = cheerio.load(body);
        $(`a`).each(function () {
            var text = $(this).text();
            var link = $(this).attr('href');
            link = stripTrailingSlash(link);
            var uniqueLink = link.split("?");
            console.log(uniqueLink);
            if(uniqueLinks[uniqueLink[0]]) {
                splitParams(uniqueLink);
                uniqueLinks[uniqueLink[0]]['count'] += 1;
            } else {
                uniqueLinks[uniqueLink[0]] = {};
                uniqueLinks[uniqueLink[0]]['count'] = 1;
                uniqueLinks[uniqueLink[0]]['params'] = {};
                splitParams(uniqueLink);
            }
            // console.log(uniqueLinks);
            if (!links[link]) {
                // console.log(link);
                links[link] = 'pending';
                queue.push(link);
            }
            
        });
        // console.log('ddfvdffd');
        links[url] = 'completed';
        concurrency--;
        return cb(null, url);
    });
};

var stripTrailingSlash = (str) => {
    if (str.substr(-1) === '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
};

var splitParams = (uniqueLink) => {
    if(uniqueLink.length > 1){
        var parameters = uniqueLink[1].split('&');
        // console.log(parameters);
        parameters.forEach(params => {
            // console.log(params);
            var param = params.split('=')[0];
            // console.log(param);
            if(!uniqueLinks[uniqueLink[0]]['params'][param]){
                uniqueLinks[uniqueLink[0]]['params'][param] = true;
            }
        });
    }
}

let url = 'https://medium.com';
links[url] = 'pending';
push(url);
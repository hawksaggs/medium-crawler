Recursively​ crawl popular blogging website ​https://medium.com​ using Node.js and harvest all possible
hyperlinks that belong to ​medium.com​ and store them in a database of your choice

What it does:
1. It store Every unique URL you encountered.
2. The total reference count of every URL.
3. A complete unique list of parameters associated with this URL.

Features:
1. Solution is ​asynchronous​ in nature.
2. It maintain a ​concurrency of 5 requests​ at all times
3. Use ​request.js​, but not its connection pool

To setup the project:
1. You need to have nodejs and mongodb installed.
2. Create .env file and add MONGODB_URL=mongodb://localhost:27017/<database-name>
3. Run node parser.js or npm start